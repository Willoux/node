const pino = require('pino')('app.log');

const http = require('http');

const server = http.createServer();

const mysql = require('mysql')

const db = mysql.createPool(    {
    host: 'localhost',
    user: 'wmehat',
    password: '13122502wM*',
    database: 'mehat_',
    connectionLimit: 10
})


const data = [
    {
        title: 'book 1'
    },
    {
        title: 'book 2'
    }
]

server.on('request', (request, response) =>  {
    if(request.url == '/book')  {
        db.query(
            'select id, title, summary from books order by id asc',
            (error, rows) => {
                if (error)  {
                    pino.info(error)
                    response.writeHead(500)
                    response.end()
                } else  {
                    response.writeHead(200, {
                        'Content-Type' : 'application.JSON',
                    })
                    response.write(JSON.stringify(rows))
                    response.end();
                }
            }
        )

    }
    else if ((m = request.url.match('/book/([0-9]+)')))   {
        if ( request.method == 'GET')   {
            db.query(
                'select id, title, summary from books where id = ?',
                [m[1]],
                (error, rows) => {
                
                    if (error)  {
                        response.writeHead(500)
                        response.end()
                    } else  if (rows.length == 0 ){
                        response.writeHead(404)
                        response.end()

                    } else  {
                        response.writeHead(200, {
                        'Content-Type' : 'application.JSON',
                        });

                        response.write(JSON.stringify(rows[0]))
                        response.end();
                    }

                
                }
            )
        } else if (request.method == 'DELETE')  {
            db.query(
                'DELETE from books where id = ?',
                [m[1]],
                (error, rows) => {
                    
                    if (error)  {
                        response.writeHead(500)
                        response.end()
                    } else  {
                        response.writeHead(200, {
                        'Content-Type' : 'application.JSON',
                        });
    
                        response.write(JSON.stringify(rows[0]))
                        response.end();
                    }    
                }
            )
        }

    } else  {

        response.writeHead(404);
        response.end();
    }
})

server.listen();


pino.info('App started')